﻿using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// Bold text <b>..</b>
    /// </summary>
    public class BoldCommand:ICommand
    {
        private const string Bold = "b";

        /// <summary>
        /// получаем 
        /// </summary>
        /// <param name="positionBegin"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public HtmlTemplate Execute(int positionBegin, int length)
        {
           var boldTag = new HtmlTag(Bold, false);
           var template = new HtmlTemplate(positionBegin, length, boldTag);
           return template;
        }
    }
}