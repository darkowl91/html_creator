﻿using System.Collections.Generic;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// Выравнивание текста по центру
    /// </summary>
    public class CenterCommand:ICommand
    {
        private const string Center = "center";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var centerTag = new HtmlTag(Center, false);
            return new HtmlTemplate(positionBegin,length,centerTag);
        }
    }
}