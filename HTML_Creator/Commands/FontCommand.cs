﻿using System.Collections.Generic;
using System.Globalization;
using HTML_Creator.Forms;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// Создфние шаблона для применения тега font
    /// </summary>
    public class FontCommand:ICommand
    {
        private const string Font = "font";
        private const string Color = "color";
        private const string Face = "face";
        private const string Size = "size";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var fontForm = new FontForm();
            HtmlTemplate template = null;
            if (fontForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var currentFont = fontForm.currentFont;
                var htmlTagAttributes = new List<HtmlTagAttribute>
                {
                    new HtmlTagAttribute(Face, currentFont.Name),
                    new HtmlTagAttribute(Size, fontForm.fontSize.ToString(CultureInfo.InvariantCulture)),
                    new HtmlTagAttribute(Color, fontForm.fontColor.Name)
                };
                var tag = new HtmlTag(Font, htmlTagAttributes, false);
                template = new HtmlTemplate(positionBegin, length, tag);    
            }
            return template;
        }
    }
}