﻿using System.Collections.Generic;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// Интерфей команды
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// Получение шаблона
        /// </summary>
        /// <param name="positionBegin">начало</param>
        /// <param name="length">длинна</param>
        /// <returns></returns>
        HtmlTemplate Execute(int positionBegin, int length);
    }
}