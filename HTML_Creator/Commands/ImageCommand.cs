﻿using System.Collections.Generic;
using HTML_Creator.Forms;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// команда для вставки изображения
    /// </summary>
    public class ImageCommand:ICommand
    {
        private const string Img = "img";
        private const string Src = "src";
        private const string Alt = "alt";
        private const string Align = "align";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            HtmlTemplate template = null;
            var imageForm = new ImageForm();
            if (imageForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var htmlTagAttributes = new List<HtmlTagAttribute>
                    {
                        new HtmlTagAttribute(Src, imageForm.ImageLink),
                        new HtmlTagAttribute(Alt, imageForm.ImageText),
                        new HtmlTagAttribute(Align, imageForm.ImageAlign.ToString())
                    };
                var imgTag = new HtmlTag(Img, htmlTagAttributes);
                template = new HtmlTemplate(positionBegin,length,imgTag);
            }
            return template;
        }
    }
}