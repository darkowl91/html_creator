﻿using System.Collections.Generic;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    public class ItalicCommand:ICommand
    {
        private const string Italic = "i";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var italicTag = new HtmlTag(Italic,false);
            var template = new HtmlTemplate(positionBegin,length,italicTag);
            return template;
        }
    }
}