﻿using System.Collections.Generic;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    public class LeftCommand:ICommand
    {
        private const string P = "p";
        private const string Align = "align";
        private const string Left = "left";
        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var atributes = new List<HtmlTagAttribute>
                {
                    new HtmlTagAttribute(Align, Left)
                };

            var leftTag = new HtmlTag(P, atributes, false);
            return new HtmlTemplate(positionBegin, length, leftTag);
        }
    }
}