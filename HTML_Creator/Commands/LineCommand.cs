﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using HTML_Creator.Forms;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// команда для вставки линии
    /// </summary>
    public class LineCommand:ICommand
    {
        private const string Hr = "hr";
        private const string Align = "align";
        private const string Width = "width";
        private const string Size = "size";
        private const string Color = "color";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            HtmlTemplate template = null;
            var form = new TagPropertiesForm(length);

            if (form.ShowDialog() == DialogResult.OK)
            {
                var atributes = new List<HtmlTagAttribute>
                    {
                        new HtmlTagAttribute(Align,form.ImageAlign.ToString()),
                        new HtmlTagAttribute(Width,form.Width.ToString(CultureInfo.InvariantCulture)),
                        new HtmlTagAttribute(Size,form.Size.ToString(CultureInfo.InvariantCulture)),
                        new HtmlTagAttribute(Color,form.Color.Name)
                    };
                var hrTag = new HtmlTag(Hr,atributes,false);
                template = new HtmlTemplate(positionBegin,length,hrTag);
            }
            return template;
        }
    }
}