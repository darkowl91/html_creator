﻿using System.Collections.Generic;
using System.Windows.Forms;
using HTML_Creator.Forms;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class LinkCommand:ICommand
    {
        private const string A = "a";
        private const string HREF = "href";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            HtmlTemplate template = null;
            var hrefForm = new HrefForm();

            if (hrefForm.ShowDialog() == DialogResult.OK)
            {
                var atributes = new List<HtmlTagAttribute>
                    {
                        new HtmlTagAttribute(HREF, hrefForm.HrefLink)
                    };
                var aTag = new HtmlTag(A, atributes,false);

                template = new HtmlTemplate(positionBegin, length, aTag);
            }
            return template;
        }
    }
}