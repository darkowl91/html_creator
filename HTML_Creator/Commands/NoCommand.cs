﻿using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// Команда отсутствует
    /// </summary>
    public class NoCommand:ICommand
    {
        public HtmlTemplate Execute(int positionBegin, int length)
        {
            return null;
        }
    }
}