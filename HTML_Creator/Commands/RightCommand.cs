﻿using System.Collections.Generic;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// выравнивание по правому краю
    /// </summary>
    public class RightCommand:ICommand
    {
        private const string P = "p";
        private const string Align = "align";
        private const string Right = "right";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var atributes = new List<HtmlTagAttribute>
                {
                    new HtmlTagAttribute(Align, Right)
                };

            var leftTag = new HtmlTag(P, atributes, false);
            return new HtmlTemplate(positionBegin, length, leftTag);
        }
    }
}