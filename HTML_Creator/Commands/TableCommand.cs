﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using HTML_Creator.Forms;
using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    /// <summary>
    /// вставка таблицы
    /// </summary>
    public class TableCommand:ICommand
    {
        private const string Table = "table";
        private const string Caption = "caption";
        private const string Aligment = "aligment";
        private const string Border = "border";
        private const string Width = "width";
        private const string Rows = "tr";
        private const string Colums = "td";
        private const string CellPadding = "cellpadding";
        private const string CellSpacing = "cellspacing";

        public HtmlTemplate Execute(int positionBegin, int length)
        {
            HtmlTemplate template = null;
            var tableForm = new TableForm();
            if (tableForm.ShowDialog() == DialogResult.OK)
            {
                //TABLE TAG CREATE
                //list of table attributes
                var tableAtributes = new List<HtmlTagAttribute>()
                    {
                        new HtmlTagAttribute(Width,
                                             tableForm.TableProperties.TableWidth.ToString(CultureInfo.InvariantCulture)),
                        new HtmlTagAttribute(Border,
                                             tableForm.TableProperties.BorderSize.ToString(CultureInfo.InvariantCulture)),
                        new HtmlTagAttribute(CellSpacing,
                                             tableForm.TableProperties.CellSpacing.ToString(CultureInfo.InvariantCulture)),
                        new HtmlTagAttribute(CellPadding,
                                             tableForm.TableProperties.CellPadding.ToString(CultureInfo.CurrentUICulture)),
                        
                    };
                var tableTag = new HtmlTag(Table, tableAtributes, false);
                template = new HtmlTemplate(positionBegin, length, tableTag);
                //rows
//                for (int i = 0; i < tableForm.TableProperties.TableRows; i++)
//                {
//                    var tr = new HtmlTag(Rows, false);
//                    template.Tags.Add(tr);
//                }
//                //colums
//                for (int i = 0; i < tableForm.TableProperties.TableColumns; i++)
//                {
//                    var td = new HtmlTag(Colums, false);
//                    template.Tags.Add(td);
//                }
            }
            return template;
        }
    }
}