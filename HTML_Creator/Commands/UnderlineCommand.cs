﻿using HTML_Creator.Html;

namespace HTML_Creator.Commands
{
    public class UnderlineCommand:ICommand
    {
        private const string U = "u";
        public HtmlTemplate Execute(int positionBegin, int length)
        {
            var uTag = new HtmlTag(U, false);
            return new HtmlTemplate(positionBegin, length, uTag);
        }
    }
}