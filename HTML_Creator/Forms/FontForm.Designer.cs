﻿using System.Drawing;

namespace HTML_Creator.Forms
{
    partial class FontForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// текущий шрифт
        /// </summary>
        public Font currentFont { get; set; }
        /// <summary>
        /// текущий цвет
        /// </summary>
        public Color fontColor { get; set;}
        /// <summary>
        /// текущий размер
        /// </summary>
        public float fontSize { get; set; }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSample = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.listFontSize = new System.Windows.Forms.ComboBox();
            this.listFontName = new System.Windows.Forms.ComboBox();
            this.labelSize = new System.Windows.Forms.Label();
            this.bApply = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buttonColor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelSample
            // 
            this.labelSample.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSample.Location = new System.Drawing.Point(12, 192);
            this.labelSample.Name = "labelSample";
            this.labelSample.Size = new System.Drawing.Size(120, 23);
            this.labelSample.TabIndex = 26;
            this.labelSample.Text = "Sample AaZa";
            this.labelSample.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(13, 9);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(120, 16);
            this.labelName.TabIndex = 25;
            this.labelName.Text = "Font Name";
            // 
            // listFontSize
            // 
            this.listFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listFontSize.FormattingEnabled = true;
            this.listFontSize.Items.AddRange(new object[] {
            "1 : 8  points",
            "2 : 10 points",
            "3 : 12 points",
            "4 : 14 points",
            "5 : 18 points",
            "6 : 24 points",
            "7 : 36 points"});
            this.listFontSize.Location = new System.Drawing.Point(138, 28);
            this.listFontSize.Name = "listFontSize";
            this.listFontSize.Size = new System.Drawing.Size(160, 21);
            this.listFontSize.TabIndex = 24;
            this.listFontSize.SelectedIndexChanged += new System.EventHandler(this.listFontSize_SelectedIndexChanged);
            // 
            // listFontName
            // 
            this.listFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.listFontName.FormattingEnabled = true;
            this.listFontName.Location = new System.Drawing.Point(12, 28);
            this.listFontName.Name = "listFontName";
            this.listFontName.Size = new System.Drawing.Size(121, 160);
            this.listFontName.TabIndex = 23;
            this.listFontName.SelectedIndexChanged += new System.EventHandler(this.listFontName_SelectedIndexChanged);
            // 
            // labelSize
            // 
            this.labelSize.Location = new System.Drawing.Point(139, 9);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(120, 16);
            this.labelSize.TabIndex = 19;
            this.labelSize.Text = "Font Size";
            // 
            // bApply
            // 
            this.bApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bApply.Location = new System.Drawing.Point(144, 192);
            this.bApply.Name = "bApply";
            this.bApply.Size = new System.Drawing.Size(75, 23);
            this.bApply.TabIndex = 15;
            this.bApply.Text = "Apply";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(225, 192);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 14;
            this.bCancel.Text = "Cancel";
            // 
            // buttonColor
            // 
            this.buttonColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColor.Location = new System.Drawing.Point(138, 55);
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.Size = new System.Drawing.Size(160, 131);
            this.buttonColor.TabIndex = 27;
            this.buttonColor.Text = "Color";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
            // 
            // FontForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 227);
            this.Controls.Add(this.buttonColor);
            this.Controls.Add(this.labelSample);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.listFontSize);
            this.Controls.Add(this.listFontName);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.bApply);
            this.Controls.Add(this.bCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(320, 261);
            this.MinimumSize = new System.Drawing.Size(320, 261);
            this.Name = "FontForm";
            this.Text = "FontForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelSample;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ComboBox listFontSize;
        private System.Windows.Forms.ComboBox listFontName;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Button bApply;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buttonColor;

    }
}