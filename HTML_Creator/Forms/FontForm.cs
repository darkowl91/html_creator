﻿using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HTML_Creator.Forms
{
    /// <summary>
    /// класс формы атрибутов шрифта
    /// </summary>
    public partial class FontForm : Form
    {
        /// <summary>
        /// конструктор поумолчанию
        /// подгружаем доступные шрифты
        /// </summary>
        public FontForm()
        {
            InitializeComponent();
            LoadFonts();
        }

         /// <summary>
        /// заполняем список шрифтов
        /// </summary>
        private void LoadFonts()
        {
            this.listFontName.SuspendLayout();

            var fonts = new InstalledFontCollection();
            foreach (var family in fonts.Families) // FontFamily.Families
            {
                if (family.IsStyleAvailable(FontStyle.Regular & FontStyle.Bold & FontStyle.Italic & FontStyle.Underline)) 
                {
                    this.listFontName.Items.Add(family.Name);
                }
            }
            this.listFontName.SelectedIndex = 0;
            this.listFontName.ResumeLayout();
        }

        /// <summary>
        /// пердпросмотр после вбора шрифта 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listFontName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetFontTextSample();
        }

        /// <summary>
        /// предпросмотр
        /// </summary>
        private void SetFontTextSample()
        {
            var fontName = ((string)this.listFontName.SelectedItem);
            fontSize = this.listFontSize.SelectedIndex;
            this.currentFont = new Font(fontName, this.Font.Size + 2);
            labelSample.ForeColor = fontColor;
            labelSample.Font = currentFont;
        }

        /// <summary>
        /// выбор цвета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonColor_Click(object sender, System.EventArgs e)
        {
            colorDialog1.ShowDialog();
            fontColor = colorDialog1.Color;
            buttonColor.BackColor = colorDialog1.Color;
            SetFontTextSample();
        }

        /// <summary>
        /// выбор размера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listFontSize_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetFontTextSample();
        }
    }
}
