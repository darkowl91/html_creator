﻿using System;
using System.Windows.Forms;
using HTML_Creator.Forms.Enums;

namespace HTML_Creator.Forms
{
    public partial class HrefForm : Form
    {

        /// <summary>
        /// 
        /// </summary>
        public HrefForm()
        {
            InitializeComponent();
            // define the text for the targets
        }

        /// <summary>
        /// Property for the href for the text
        /// </summary>
        public string HrefLink
        {
            get
            {
                return this.hrefLink.Text.Trim();
            }
            set
            {
                this.hrefLink.Text = value.Trim();
            }

        }
    }
}
