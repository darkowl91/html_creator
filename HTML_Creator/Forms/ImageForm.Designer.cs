﻿namespace HTML_Creator.Forms
{
    partial class ImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listAlign = new System.Windows.Forms.ComboBox();
            this.labelAlign = new System.Windows.Forms.Label();
            this.hrefLink = new System.Windows.Forms.TextBox();
            this.hrefText = new System.Windows.Forms.TextBox();
            this.labelHref = new System.Windows.Forms.Label();
            this.labelText = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bInsert = new System.Windows.Forms.Button();
            this.btnBowseImage = new System.Windows.Forms.Button();
            this.openImageFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // listAlign
            // 
            this.listAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listAlign.FormattingEnabled = true;
            this.listAlign.Location = new System.Drawing.Point(44, 84);
            this.listAlign.Name = "listAlign";
            this.listAlign.Size = new System.Drawing.Size(121, 21);
            this.listAlign.TabIndex = 10;
            // 
            // labelAlign
            // 
            this.labelAlign.Location = new System.Drawing.Point(4, 84);
            this.labelAlign.Name = "labelAlign";
            this.labelAlign.Size = new System.Drawing.Size(32, 23);
            this.labelAlign.TabIndex = 15;
            this.labelAlign.Text = "Align:";
            // 
            // hrefLink
            // 
            this.hrefLink.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hrefLink.Location = new System.Drawing.Point(44, 12);
            this.hrefLink.Name = "hrefLink";
            this.hrefLink.Size = new System.Drawing.Size(230, 20);
            this.hrefLink.TabIndex = 8;
            // 
            // hrefText
            // 
            this.hrefText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hrefText.Location = new System.Drawing.Point(44, 44);
            this.hrefText.Name = "hrefText";
            this.hrefText.Size = new System.Drawing.Size(292, 20);
            this.hrefText.TabIndex = 9;
            // 
            // labelHref
            // 
            this.labelHref.Location = new System.Drawing.Point(4, 12);
            this.labelHref.Name = "labelHref";
            this.labelHref.Size = new System.Drawing.Size(32, 23);
            this.labelHref.TabIndex = 12;
            this.labelHref.Text = "Href:";
            // 
            // labelText
            // 
            this.labelText.Location = new System.Drawing.Point(4, 44);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(32, 23);
            this.labelText.TabIndex = 11;
            this.labelText.Text = "Text:";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(263, 118);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 14;
            this.bCancel.Text = "Cancel";
            // 
            // bInsert
            // 
            this.bInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bInsert.Location = new System.Drawing.Point(175, 118);
            this.bInsert.Name = "bInsert";
            this.bInsert.Size = new System.Drawing.Size(80, 23);
            this.bInsert.TabIndex = 13;
            this.bInsert.Text = "Insert Image";
            // 
            // btnBowseImage
            // 
            this.btnBowseImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnBowseImage.Location = new System.Drawing.Point(280, 8);
            this.btnBowseImage.Name = "btnBowseImage";
            this.btnBowseImage.Size = new System.Drawing.Size(56, 24);
            this.btnBowseImage.TabIndex = 16;
            this.btnBowseImage.Text = "Browse";
            this.btnBowseImage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBowseImage.UseVisualStyleBackColor = true;
            this.btnBowseImage.Click += new System.EventHandler(this.btnBowseImage_Click);
            // 
            // openImageFileDialog
            // 
            this.openImageFileDialog.FileName = "image";
            this.openImageFileDialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Fi" +
    "les (*.gif)|*.gif";
            // 
            // ImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 153);
            this.Controls.Add(this.btnBowseImage);
            this.Controls.Add(this.listAlign);
            this.Controls.Add(this.labelAlign);
            this.Controls.Add(this.hrefLink);
            this.Controls.Add(this.hrefText);
            this.Controls.Add(this.labelHref);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bInsert);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(364, 187);
            this.MinimumSize = new System.Drawing.Size(364, 187);
            this.Name = "ImageForm";
            this.Text = "ImageForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox listAlign;
        private System.Windows.Forms.Label labelAlign;
        private System.Windows.Forms.TextBox hrefLink;
        private System.Windows.Forms.TextBox hrefText;
        private System.Windows.Forms.Label labelHref;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bInsert;
        private System.Windows.Forms.Button btnBowseImage;
        private System.Windows.Forms.OpenFileDialog openImageFileDialog;
    }
}