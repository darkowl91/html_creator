﻿using System;
using System.Windows.Forms;
using HTML_Creator.Forms.Enums;

namespace HTML_Creator.Forms
{
    /// <summary>
    /// Форма для вставки картинки
    /// </summary>
    public partial class ImageForm : Form
    {
       
        /// <summary>
        /// конструктор формы
        /// </summary>
        public ImageForm()
        {
            InitializeComponent();
            // define the text for the alignment
            this.listAlign.Items.AddRange(Enum.GetNames(typeof(ImageAlignOption)));
            // ensure default value set for target
            this.listAlign.SelectedIndex = 4;
        }

        /// <summary>
        /// Property for the text to display
        /// </summary>
        public string ImageText
        {
            get
            {
                return this.hrefText.Text;
            }
            set
            {
                this.hrefText.Text = value;
            }

        } //ImageText
        /// <summary>
        /// Property for the href for the image
        /// </summary>
        public string ImageLink
        {
            get
            {
                return this.hrefLink.Text.Trim();
            }
            set
            {
                this.hrefLink.Text = value.Trim();
            }

        }

        /// <summary>
        /// Property for the image align
        /// </summary>
        public ImageAlignOption ImageAlign
        {
            get
            {
                return (ImageAlignOption)this.listAlign.SelectedIndex;
            }
            set
            {
                this.listAlign.SelectedIndex = (int)value;
            }
        }

        /// <summary>
        /// открыть картинку из файла 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBowseImage_Click(object sender, EventArgs e)
        {
            if (openImageFileDialog.ShowDialog() == DialogResult.OK)
            {
                hrefLink.Text = openImageFileDialog.FileName;
            }
        }
    }
}
