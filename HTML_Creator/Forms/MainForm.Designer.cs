﻿using System.Collections.Generic;
using HTML_Creator.Html;
using HTML_Creator.util;

namespace HTML_Creator.Forms
{
    /// <summary>
    /// main form
    /// </summary>
    partial class HtmlCreatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HtmlCreatorForm));
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.FileButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.openApplyTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.saveTemplateToXmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveHtmlPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutButton = new System.Windows.Forms.ToolStripButton();
            this.buttonApply = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DocumenttreeView = new System.Windows.Forms.TreeView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBoxSource = new ScintillaNET.Scintilla();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBoxTemplate = new ScintillaNET.Scintilla();
            this.contextMenuSourceText = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BoldItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FontMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ItalicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CenteerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LeftMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.LineMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TableMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UnderlineMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LinkMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTemplateFiledialog = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuTreeView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editTagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteTagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxTemplate)).BeginInit();
            this.contextMenuSourceText.SuspendLayout();
            this.contextMenuTreeView.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 366);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(734, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // toolBar
            // 
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileButton,
            this.AboutButton});
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(734, 25);
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "toolStrip1";
            // 
            // FileButton
            // 
            this.FileButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openApplyTemplateToolStripMenuItem,
            this.toolStripSeparator4,
            this.saveTemplateToXmlToolStripMenuItem,
            this.saveHtmlPageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.FileButton.Image = global::HTML_Creator.Properties.Resources.FilePNG;
            this.FileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FileButton.Name = "FileButton";
            this.FileButton.Size = new System.Drawing.Size(54, 22);
            this.FileButton.Text = "File";
            this.FileButton.ToolTipText = " Menu File";
            // 
            // openApplyTemplateToolStripMenuItem
            // 
            this.openApplyTemplateToolStripMenuItem.Name = "openApplyTemplateToolStripMenuItem";
            this.openApplyTemplateToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.openApplyTemplateToolStripMenuItem.Text = "Open and apply template";
            this.openApplyTemplateToolStripMenuItem.Click += new System.EventHandler(this.openApplyTemplateToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(202, 6);
            // 
            // saveTemplateToXmlToolStripMenuItem
            // 
            this.saveTemplateToXmlToolStripMenuItem.Name = "saveTemplateToXmlToolStripMenuItem";
            this.saveTemplateToXmlToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.saveTemplateToXmlToolStripMenuItem.Text = "Save template to xml";
            this.saveTemplateToXmlToolStripMenuItem.Click += new System.EventHandler(this.saveTemplateToXmlToolStripMenuItem_Click);
            // 
            // saveHtmlPageToolStripMenuItem
            // 
            this.saveHtmlPageToolStripMenuItem.Name = "saveHtmlPageToolStripMenuItem";
            this.saveHtmlPageToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.saveHtmlPageToolStripMenuItem.Text = "Save Html page";
            this.saveHtmlPageToolStripMenuItem.Click += new System.EventHandler(this.saveHtmlPageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Image = global::HTML_Creator.Properties.Resources.AboutPNG;
            this.AboutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(59, 22);
            this.AboutButton.Text = "About";
            this.AboutButton.ToolTipText = "About project\r\n";
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Image = global::HTML_Creator.Properties.Resources.applyPNG;
            this.buttonApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonApply.Location = new System.Drawing.Point(594, 333);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(128, 30);
            this.buttonApply.TabIndex = 5;
            this.buttonApply.Text = "Apply changes";
            this.buttonApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(710, 299);
            this.splitContainer1.SplitterDistance = 236;
            this.splitContainer1.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.DocumenttreeView);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 293);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Document tree";
            // 
            // DocumenttreeView
            // 
            this.DocumenttreeView.BackColor = System.Drawing.SystemColors.Info;
            this.DocumenttreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DocumenttreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DocumenttreeView.Location = new System.Drawing.Point(3, 16);
            this.DocumenttreeView.Name = "DocumenttreeView";
            this.DocumenttreeView.Size = new System.Drawing.Size(224, 274);
            this.DocumenttreeView.TabIndex = 0;
            this.DocumenttreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.DocumenttreeView_NodeMouseClick);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(470, 299);
            this.splitContainer2.SplitterDistance = 174;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.richTextBoxSource);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(464, 168);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text";
            // 
            // richTextBoxSource
            // 
            this.richTextBoxSource.BackColor = System.Drawing.SystemColors.Info;
            this.richTextBoxSource.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxSource.ConfigurationManager.Language = "html";
            this.richTextBoxSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxSource.EndOfLine.IsVisible = true;
            this.richTextBoxSource.Folding.MarkerScheme = ScintillaNET.FoldMarkerScheme.CirclePlusMinus;
            this.richTextBoxSource.Folding.UseCompactFolding = true;
            this.richTextBoxSource.ForeColor = System.Drawing.SystemColors.ControlText;
            this.richTextBoxSource.LineWrapping.IndentMode = ScintillaNET.LineWrappingIndentMode.Indent;
            this.richTextBoxSource.LineWrapping.Mode = ScintillaNET.LineWrappingMode.Word;
            this.richTextBoxSource.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxSource.Margins.Margin0.Width = 40;
            this.richTextBoxSource.Name = "richTextBoxSource";
            this.richTextBoxSource.Size = new System.Drawing.Size(458, 149);
            this.richTextBoxSource.Styles.LineNumber.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.richTextBoxSource.Styles.LineNumber.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.richTextBoxSource.TabIndex = 0;
            this.richTextBoxSource.MouseUp += new System.Windows.Forms.MouseEventHandler(this.richTextBoxSource_MouseUp);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(464, 115);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "HTML ";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(455, 94);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.webBrowser);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(447, 65);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Page";
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(3, 3);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(441, 59);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser_DocumentCompleted);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.richTextBoxTemplate);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(447, 65);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Template";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // richTextBoxTemplate
            // 
            this.richTextBoxTemplate.BackColor = System.Drawing.SystemColors.Info;
            this.richTextBoxTemplate.ConfigurationManager.Language = "html";
            this.richTextBoxTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxTemplate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.richTextBoxTemplate.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxTemplate.Margins.Margin0.Width = 40;
            this.richTextBoxTemplate.Name = "richTextBoxTemplate";
            this.richTextBoxTemplate.Size = new System.Drawing.Size(441, 59);
            this.richTextBoxTemplate.TabIndex = 0;
            // 
            // contextMenuSourceText
            // 
            this.contextMenuSourceText.AllowDrop = true;
            this.contextMenuSourceText.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BoldItemMenu,
            this.FontMenuItem,
            this.ImageMenuItem,
            this.ItalicMenuItem,
            this.toolStripSeparator1,
            this.CenteerMenuItem,
            this.LeftMenuItem,
            this.RightMenuItem,
            this.toolStripSeparator2,
            this.LineMenuItem,
            this.TableMenuItem,
            this.UnderlineMenuItem,
            this.LinkMenuItem});
            this.contextMenuSourceText.Name = "contextMenuSourceText";
            this.contextMenuSourceText.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuSourceText.Size = new System.Drawing.Size(126, 258);
            // 
            // BoldItemMenu
            // 
            this.BoldItemMenu.Image = global::HTML_Creator.Properties.Resources.ToolbarBold;
            this.BoldItemMenu.Name = "BoldItemMenu";
            this.BoldItemMenu.Size = new System.Drawing.Size(125, 22);
            this.BoldItemMenu.Text = "Bold";
            this.BoldItemMenu.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // FontMenuItem
            // 
            this.FontMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarFontStyle;
            this.FontMenuItem.Name = "FontMenuItem";
            this.FontMenuItem.Size = new System.Drawing.Size(125, 22);
            this.FontMenuItem.Text = "Font";
            this.FontMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // ImageMenuItem
            // 
            this.ImageMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarImage;
            this.ImageMenuItem.Name = "ImageMenuItem";
            this.ImageMenuItem.Size = new System.Drawing.Size(125, 22);
            this.ImageMenuItem.Text = "Image";
            this.ImageMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // ItalicMenuItem
            // 
            this.ItalicMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarItalic;
            this.ItalicMenuItem.Name = "ItalicMenuItem";
            this.ItalicMenuItem.Size = new System.Drawing.Size(125, 22);
            this.ItalicMenuItem.Text = "Italic";
            this.ItalicMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(122, 6);
            // 
            // CenteerMenuItem
            // 
            this.CenteerMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarJustifyCenter;
            this.CenteerMenuItem.Name = "CenteerMenuItem";
            this.CenteerMenuItem.Size = new System.Drawing.Size(125, 22);
            this.CenteerMenuItem.Text = "Center";
            this.CenteerMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // LeftMenuItem
            // 
            this.LeftMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarJustifyLeft;
            this.LeftMenuItem.Name = "LeftMenuItem";
            this.LeftMenuItem.Size = new System.Drawing.Size(125, 22);
            this.LeftMenuItem.Text = "Left";
            this.LeftMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // RightMenuItem
            // 
            this.RightMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarJustifyRight;
            this.RightMenuItem.Name = "RightMenuItem";
            this.RightMenuItem.Size = new System.Drawing.Size(125, 22);
            this.RightMenuItem.Text = "Right";
            this.RightMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(122, 6);
            // 
            // LineMenuItem
            // 
            this.LineMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarLine;
            this.LineMenuItem.Name = "LineMenuItem";
            this.LineMenuItem.Size = new System.Drawing.Size(125, 22);
            this.LineMenuItem.Text = "Line";
            this.LineMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // TableMenuItem
            // 
            this.TableMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarTable;
            this.TableMenuItem.Name = "TableMenuItem";
            this.TableMenuItem.Size = new System.Drawing.Size(125, 22);
            this.TableMenuItem.Text = "Table";
            this.TableMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // UnderlineMenuItem
            // 
            this.UnderlineMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarUnderline;
            this.UnderlineMenuItem.Name = "UnderlineMenuItem";
            this.UnderlineMenuItem.Size = new System.Drawing.Size(125, 22);
            this.UnderlineMenuItem.Text = "Underline";
            this.UnderlineMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // LinkMenuItem
            // 
            this.LinkMenuItem.Image = global::HTML_Creator.Properties.Resources.ToolbarWebLink;
            this.LinkMenuItem.Name = "LinkMenuItem";
            this.LinkMenuItem.Size = new System.Drawing.Size(125, 22);
            this.LinkMenuItem.Text = "WebLink";
            this.LinkMenuItem.Click += new System.EventHandler(this.MenuItem_Click);
            // 
            // openTemplateFiledialog
            // 
            this.openTemplateFiledialog.FileName = "templae.xml";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Image = global::HTML_Creator.Properties.Resources.previewePNG;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(460, 333);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 30);
            this.button1.TabIndex = 9;
            this.button1.Text = "Preview page\r\n";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonPreView_Click);
            // 
            // contextMenuTreeView
            // 
            this.contextMenuTreeView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editTagToolStripMenuItem,
            this.toolStripSeparator3,
            this.deleteTagToolStripMenuItem});
            this.contextMenuTreeView.Name = "contextMenuTreeView";
            this.contextMenuTreeView.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuTreeView.Size = new System.Drawing.Size(128, 54);
            // 
            // editTagToolStripMenuItem
            // 
            this.editTagToolStripMenuItem.Name = "editTagToolStripMenuItem";
            this.editTagToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.editTagToolStripMenuItem.Text = "Edit tag";
            this.editTagToolStripMenuItem.Click += new System.EventHandler(this.editTagToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(124, 6);
            // 
            // deleteTagToolStripMenuItem
            // 
            this.deleteTagToolStripMenuItem.Name = "deleteTagToolStripMenuItem";
            this.deleteTagToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.deleteTagToolStripMenuItem.Text = "Delete tag";
            this.deleteTagToolStripMenuItem.Click += new System.EventHandler(this.editTagToolStripMenuItem_Click);
            // 
            // HtmlCreatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 388);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HtmlCreatorForm";
            this.Text = "HTML Creator";
            this.Load += new System.EventHandler(this.htmlCreator_Form_Load);
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.richTextBoxTemplate)).EndInit();
            this.contextMenuSourceText.ResumeLayout(false);
            this.contextMenuTreeView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.ToolStripDropDownButton FileButton;
        private System.Windows.Forms.ToolStripButton AboutButton;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView DocumenttreeView;
        private System.Windows.Forms.ContextMenuStrip contextMenuSourceText;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.ToolStripMenuItem BoldItemMenu;
        private System.Windows.Forms.ToolStripMenuItem FontMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ItalicMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CenteerMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LeftMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RightMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LineMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TableMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UnderlineMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LinkMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        //#
        private readonly List<HtmlTemplate> _htmlTemplates = new List<HtmlTemplate>();
        private readonly TextHelper _textHelper = TextHelper.GetInstance();
        private readonly CommandHelper _commandHelper = CommandHelper.GetInstance();
        private ScintillaNET.Scintilla richTextBoxSource;
        private ScintillaNET.Scintilla richTextBoxTemplate;
        private readonly HtmlTreeBuilder _builder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem openApplyTemplateToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openTemplateFiledialog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuTreeView;
        private System.Windows.Forms.ToolStripMenuItem editTagToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem deleteTagToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem saveTemplateToXmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveHtmlPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

