﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HTML_Creator.Html;
using HTML_Creator.Properties;
using HTML_Creator.util;

namespace HTML_Creator.Forms
{
    public partial class HtmlCreatorForm : Form
    {
        /// <summary>
        /// получить или записать текст шаблона 
        /// </summary>
        public string ResultXml { get; set; }

        /// <summary>
        ///получить html текст
        /// </summary>
        public string Htmltext
        {
            get { return webBrowser.DocumentText; }
        }

        private HtmlTemplate _selectedTemplate;
        private int _indexTemplate = -1;

        #region formInit
        /// <summary>
        /// конструктор формы по умолчаниию
        /// </summary>
        public HtmlCreatorForm()
        {
            InitializeComponent();
            _builder = new HtmlTreeBuilder();
        }

        /// <summary>
        /// перегруженный конструктор формы для передачи текста 
        /// при инициализации формы
        /// </summary>
        /// <param name="inputData">тектовая чтрока</param>
        public HtmlCreatorForm(string inputData)
        {
            InitializeComponent();
            _textHelper.TextData = inputData;
            _textHelper.SourceData = inputData;
            _builder = new HtmlTreeBuilder();
        }

        /// <summary>
        /// перегруженный конструктор формы для передачи текста и шаблона форматирования 
        /// при инициализации формы
        /// </summary>
        /// <param name="inputData">текст</param>
        /// <param name="xmlStr">строка xml шаблона</param>
        public HtmlCreatorForm(string inputData, string xmlStr)
        {
            InitializeComponent();
            _textHelper.TextData = inputData;
            _textHelper.SourceData = inputData;
            _builder = new HtmlTreeBuilder();
            _htmlTemplates = TemplateSaver.OpenFromXmlStr(xmlStr);
        }


        /// <summary>
        /// загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void htmlCreator_Form_Load(object sender, EventArgs e)
        {
            richTextBoxSource.Text = _textHelper.TextData;
        }
        #endregion

        #region tagAdd
        /// <summary>
        /// выделение текста 
        /// вывод меню
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBoxSource_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var selectedText = richTextBoxSource.Selection.Text;
                if (!string.IsNullOrWhiteSpace(selectedText))
                {
                    contextMenuSourceText.Show(richTextBoxSource, e.X, e.Y);
                }
            }
        }

        /// <summary>
        /// Выбор тега в меню и его применеие
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            //получаем текущую команду
            var menuItemName = item.Name;
            var command = _commandHelper.GetCommand(menuItemName);
           
            //используем memoryStream для удобства копирования куска массива байтов
            var memStream = new MemoryStream();
            memStream.Write(Encoding.UTF8.GetBytes(richTextBoxSource.Text), 0, richTextBoxSource.Selection.Start);
            //оределяем идекс символа в данной координате
            var startPositionIndex = Encoding.UTF8.GetString(memStream.ToArray()).Length;
            memStream.Close();
            //получаем длинну выделенного текста
            var length = richTextBoxSource.Selection.Text.Length;
            //получаем шаблон
            var template = command.Execute(startPositionIndex, length);
            //если шаблон успешно создан 
            if (template != null)
            {
                //добовляем полученный шаблон в список шаблонов
                _htmlTemplates.Add(template);
                richTextBoxTemplate.AppendText(template + "\n");
                //выводим полученную страницу
                _textHelper.TextData = template.ApplyTemplate(_textHelper.TextData);
                webBrowser.DocumentText = _textHelper.TextData;
                richTextBoxSource.Text = _textHelper.TextData;
            }
        }
        #endregion

        #region preview
        private void buttonPreView_Click(object sender, EventArgs e)
        {
            var fileName = GenerateFileName();
            var sw = File.CreateText(fileName);
            File.SetAttributes(fileName, FileAttributes.Hidden);

            sw.WriteLine(Resources.Html_meta + webBrowser.DocumentText.ToString(CultureInfo.InvariantCulture));
            sw.Close();

            var proc = Process.Start(fileName);
            while (proc != null && !proc.HasExited)
            {

            }

            File.Delete(fileName);
        }

        private string GenerateFileName()
        {
            const string collection = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random((int)DateTime.Now.Ticks);
            var name = "";
            for (int i = 0; i < 6; i++)
            {
                int index = random.Next(collection.Length);
                name += collection[index];
            }
            return name + ".html";
        }
        #endregion

        #region updateTreeview
        private void UpdateTreeView()
        {
            DocumenttreeView.Nodes.Clear();
            if (webBrowser.Document != null)
            {
                var tree = _builder.GetTree(webBrowser.Document);
                DocumenttreeView.Nodes.Add(tree);
                DocumenttreeView.ExpandAll();
            }

        }

        /// <summary>
        /// обновление дерева тегов после загрузки окумента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            UpdateTreeView();
        }
        #endregion

        #region TreeView_navigation
        private void DocumenttreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            var sortedHtmlTemplates = _htmlTemplates.OrderBy(x => x.PositionBegin).ToList();
            var index = 0;
            int.TryParse(e.Node.Name, out index);
            _selectedTemplate = null;
            _indexTemplate = -1;

            if (index >= 4)
            {
                _indexTemplate = index - 4;
                _selectedTemplate = sortedHtmlTemplates[_indexTemplate];
                
                var begin = _selectedTemplate.PositionBegin;

                var tagLength = 2 * _selectedTemplate.HtmlTag.ToString().Length;
                var tokenLength = _selectedTemplate.TokenLength;
                var attrLength = 0;

                var htmlTagAttributes = _selectedTemplate.HtmlTag.Attributes;

                if (htmlTagAttributes != null)
                {
                    attrLength = htmlTagAttributes.Sum(htmlTagAttribute => htmlTagAttribute.ToString().Length);
                }

                var end = tagLength + tokenLength + attrLength;

                richTextBoxSource.Selection.Start =
                    Encoding.UTF8.GetBytes(richTextBoxSource.Text.Substring(0, begin)).Length;
                richTextBoxSource.Selection.End =
                    Encoding.UTF8.GetBytes(richTextBoxSource.Text.Substring(0, begin)).Length +
                    Encoding.UTF8.GetBytes(richTextBoxSource.Text.Substring(begin, end)).Length;

                //клик правой кнопкой выод меню для свойств тега 
                if (e.Button == MouseButtons.Right)
                {
                    contextMenuTreeView.Show(DocumenttreeView, e.X, e.Y);
                }
            }
        }

        /// <summary>
        /// редактирование выбранного тега
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editTagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_selectedTemplate != null && _indexTemplate != -1)
            {
                switch (((ToolStripMenuItem)sender).Name)
                {
                    case "editTagToolStripMenuItem":
                        MessageBox.Show(Resources.HtmlCreatorForm_editTagToolStripMenuItem_Click_Not_supotrted_yet_);
                        break;
                    case "deleteTagToolStripMenuItem":
                        for (var i = _indexTemplate + 1; i < _htmlTemplates.Count; i++)
                        {
                            //изменяем индекс предыдущих тегов
                            if (_htmlTemplates[i].PositionBegin > _htmlTemplates[_indexTemplate].PositionBegin)
                            {
                                _htmlTemplates[i].PositionBegin -= 2*_htmlTemplates[_indexTemplate].HtmlTag.ToString().Length;
                            }
                        }

                        _htmlTemplates.Remove(_selectedTemplate);
                        _textHelper.TextData = _textHelper.SourceData;
                        richTextBoxTemplate.ResetText();  

                        foreach (var htmlTemplate in _htmlTemplates)
                        {
                            htmlTemplate.HtmlTag.Isclosed = false;
                            richTextBoxTemplate.AppendText(htmlTemplate + "\n");
                            _textHelper.TextData = htmlTemplate.ApplyTemplate(_textHelper.TextData);
                        }
                        webBrowser.DocumentText = _textHelper.TextData;
                        richTextBoxSource.Text = _textHelper.TextData;
                        break;
                }
            }
        }
        #endregion

        #region Apply_Template save it into xml string
        /// <summary>
        /// применить изменеия 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonApply_Click(object sender, EventArgs e)
        {
            ResultXml = TemplateSaver.SaveToXml(_htmlTemplates);
            if (MessageBox.Show(Resources.HtmlCreatorForm_buttonApply_Click_Generation_Finish___,
                                Resources.HtmlCreatorForm_buttonApply_Click_Template_generation_,
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Question) == DialogResult.OK)
            {
                MessageBox.Show(ResultXml);
            }
            DialogResult = DialogResult.OK;
            Close();
        }
        #endregion

        #region Apply_Template open it from xml file
        /// <summary>
        /// открыть и применить шаблон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openApplyTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openTemplateFiledialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = openTemplateFiledialog.FileName;
                var objReader = File.OpenText(fileName);
                var xmlStr = objReader.ReadToEnd();
                var templlates = TemplateSaver.OpenFromXmlStr(xmlStr);

                foreach (var htmlTemplate in templlates)
                {
                    htmlTemplate.HtmlTag.Isclosed = false;
                    _textHelper.TextData = htmlTemplate.ApplyTemplate(_textHelper.TextData);
                    webBrowser.DocumentText = _textHelper.TextData;
                    richTextBoxSource.Text = _textHelper.TextData;
                }
            }
        }
        #endregion

        #region Menu File work
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveTemplateToXmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var filename = saveFileDialog.FileName;
                var sw = File.CreateText(filename);
                sw.WriteLine(TemplateSaver.SaveToXml(_htmlTemplates));
                sw.Close();
            }
        }


        private void saveHtmlPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var filename = saveFileDialog.FileName;
                var sw = File.CreateText(filename);
                sw.WriteLine(Resources.Html_meta + webBrowser.DocumentText.ToString(CultureInfo.InvariantCulture));
                sw.Close();
            }
        }
        #endregion
    }
}
