﻿namespace HTML_Creator.Forms
{
    partial class TagPropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listAlign = new System.Windows.Forms.ComboBox();
            this.labelAlign = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericWidth = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericSize = new System.Windows.Forms.NumericUpDown();
            this.btnColor = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.bCancel = new System.Windows.Forms.Button();
            this.bInsert = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSize)).BeginInit();
            this.SuspendLayout();
            // 
            // listAlign
            // 
            this.listAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listAlign.FormattingEnabled = true;
            this.listAlign.Location = new System.Drawing.Point(45, 12);
            this.listAlign.Name = "listAlign";
            this.listAlign.Size = new System.Drawing.Size(121, 21);
            this.listAlign.TabIndex = 16;
            // 
            // labelAlign
            // 
            this.labelAlign.Location = new System.Drawing.Point(5, 12);
            this.labelAlign.Name = "labelAlign";
            this.labelAlign.Size = new System.Drawing.Size(41, 23);
            this.labelAlign.TabIndex = 17;
            this.labelAlign.Text = "Align:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "Width:";
            // 
            // numericWidth
            // 
            this.numericWidth.Location = new System.Drawing.Point(45, 40);
            this.numericWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericWidth.Name = "numericWidth";
            this.numericWidth.Size = new System.Drawing.Size(120, 20);
            this.numericWidth.TabIndex = 20;
            this.numericWidth.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericWidth.ValueChanged += new System.EventHandler(this.numericWidth_ValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 23);
            this.label2.TabIndex = 21;
            this.label2.Text = "Size:";
            // 
            // numericSize
            // 
            this.numericSize.Location = new System.Drawing.Point(45, 68);
            this.numericSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSize.Name = "numericSize";
            this.numericSize.Size = new System.Drawing.Size(120, 20);
            this.numericSize.TabIndex = 22;
            this.numericSize.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericSize.ValueChanged += new System.EventHandler(this.numericSize_ValueChanged);
            // 
            // btnColor
            // 
            this.btnColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnColor.Location = new System.Drawing.Point(8, 97);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(157, 23);
            this.btnColor.TabIndex = 23;
            this.btnColor.Text = "Color";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(94, 142);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 25;
            this.bCancel.Text = "Cancel";
            // 
            // bInsert
            // 
            this.bInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bInsert.Location = new System.Drawing.Point(8, 142);
            this.bInsert.Name = "bInsert";
            this.bInsert.Size = new System.Drawing.Size(80, 23);
            this.bInsert.TabIndex = 24;
            this.bInsert.Text = "OK";
            // 
            // TagPropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(181, 177);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bInsert);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.numericSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listAlign);
            this.Controls.Add(this.labelAlign);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TagPropertiesForm";
            this.Text = "Tag Properties";
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox listAlign;
        private System.Windows.Forms.Label labelAlign;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericSize;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bInsert;
    }
}