﻿using System;
using System.Drawing;
using System.Windows.Forms;
using HTML_Creator.Forms.Enums;

namespace HTML_Creator.Forms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TagPropertiesForm : Form
    {
        
        /// <summary>
        /// 
        /// </summary>
        public TagPropertiesForm(int length)
        {
            InitializeComponent();
            // define the text for the alignment
            this.listAlign.Items.AddRange(Enum.GetNames(typeof(ImageAlignOption)));
            // ensure default value set for target
            this.listAlign.SelectedIndex = 4;
            Size = (int) numericSize.Value;
            numericWidth.Value = length;
            Width = (int) numericWidth.Value;
            Color = Color.Black;
        }

        /// <summary>
        /// текущий цвет
        /// </summary>
        public Color Color { get; set; }
        /// <summary>
        /// текущий размер
        /// </summary>
        public int Size { get; set; }
        /// <summary>
        /// текущая ширина
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Property for the image align
        /// </summary>
        public ImageAlignOption ImageAlign
        {
            get
            {
                return (ImageAlignOption)this.listAlign.SelectedIndex;
            }
            set
            {
                this.listAlign.SelectedIndex = (int)value;
            }
        }
        

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = colorDialog.Color;
                Color = colorDialog.Color;
            }
        }

        private void numericWidth_ValueChanged(object sender, EventArgs e)
        {
            Width = (int) numericWidth.Value;
        }

        private void numericSize_ValueChanged(object sender, EventArgs e)
        {
            Size = (int) numericSize.Value;
        }
    }
}
