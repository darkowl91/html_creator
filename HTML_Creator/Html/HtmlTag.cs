﻿using System;
using System.Collections.Generic;

namespace HTML_Creator.Html
{
    /// <summary>
    /// Клас HTML тег пердставляет сущность тега с его параметрами 
    /// </summary>
    public class HtmlTag:Tag
    {
        public HtmlTag()
        {
        }

        /// <summary>
        /// закрыт или открыт
        /// </summary>
        public bool Isclosed { get; set; }

        /// <summary>
        /// одиночный закрытый тег
        /// </summary>
        private readonly bool _isSingle;

        /// <summary>
        /// Список атрибутов тега
        /// </summary>
        public List<HtmlTagAttribute> Attributes { get; set; }

        /// <summary>
        /// создание html тега
        /// </summary>
        /// <param name="name">имя тега</param>
        /// <param name="attributes">список атрибутов</param>
        /// <param name="isclosed">открывающий или закрывающий</param>
        public HtmlTag(string name, List<HtmlTagAttribute> attributes, bool isclosed) : base(name)
        {
            Attributes = attributes;
            Isclosed = isclosed;
            _isSingle = false;//значение по умолчанию
        }

        /// <summary>
        /// создание одиночного html тега
        /// </summary>
        /// <param name="name">имя тега</param>
        /// <param name="attributes">атрибуты</param>
        public HtmlTag(string name,  List<HtmlTagAttribute> attributes) : base(name)
        {
             Isclosed = true;//значение по умолчанию
            _isSingle = true;//значение по умолчанию
            Attributes = attributes;
        }

        /// <summary>
        /// создание тега без атрибутов
        /// </summary>
        /// <param name="name">имя тега</param>
        /// <param name="isclosed">открывающий или закрывающий</param>
        public HtmlTag(string name, bool isclosed) : base(name)
        {
            Isclosed = isclosed;
        }

        /// <summary>
        /// получение текстового представления атрибутов тега
        /// </summary>
        /// <returns>строка атрибутов</returns>
        private string GetAtributeStr()
        {
            var attributesStr="";
            if (Attributes != null)
            {
                foreach (var attribute in Attributes)
                {
                    attributesStr += attribute;

                }
            }
            return attributesStr;
        }

        /// <summary>
        /// toString
        /// </summary>
        /// <returns>строка тега</returns>
        public override string ToString()
        {
            //одиночный закрытый
            if (Isclosed && _isSingle)
            {
                return "<" + base.ToString() + " " + GetAtributeStr() + "/>";
            }
            //закрытый
            if (Isclosed && !_isSingle)
            {
                return "</" + base.ToString() +  ">";
            }
            //открытый
            return "<" + base.ToString() + " " + GetAtributeStr() + ">";
        }
    }
}