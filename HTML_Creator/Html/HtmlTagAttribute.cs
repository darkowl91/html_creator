﻿using System;

namespace HTML_Creator.Html
{
    /// <summary>
    /// Атрибут html тега
    /// </summary>
    [Serializable]
    public class HtmlTagAttribute:Tag
    {
        public HtmlTagAttribute()
        {
        }

        /// <summary>
        /// создание атрибута
        /// </summary>
        /// <param name="name">имя атрибута</param>
        /// <param name="value">значение атрибута</param>
        public HtmlTagAttribute(string name, string value) : base(name)
        {
            Value = value;
        }

        /// <summary>
        /// значение атрибута
        /// </summary>
        public string Value { get; set; }

        public override string ToString()
        {
            return base.ToString() + "=" + '"' + Value + '"';
        }
    }
}