﻿﻿using System;

namespace HTML_Creator.Html
{
    /// <summary>
    /// класс Шаблон Html  
    /// </summary>
    public class HtmlTemplate
    {
       

        public HtmlTemplate()
        {
        }

        /// <summary>
        /// Шаблон Html для токена
        /// </summary>
        /// <param name="positionBegin">позиция начала</param>
        /// <param name="tokenLength">длинна токена</param>
        /// <param name="htmlTag">тег html</param>
        public HtmlTemplate(int positionBegin, int tokenLength, HtmlTag htmlTag)
        {
            PositionBegin = positionBegin;
            TokenLength = tokenLength;
            HtmlTag = htmlTag;
        }

        /// <summary>
        /// позиуия начала вставки тега
        /// </summary>
        public int PositionBegin { get; set; }

        /// <summary>
        /// длинна токена
        /// </summary>
        public int TokenLength { get; set; }
        
        private int PositionEnd()
        {
          return PositionBegin + TokenLength;
        }

        /// <summary>
        /// Html тег
        /// </summary>
        public HtmlTag HtmlTag { get; set; }

        /// <summary>
        /// применить шаблон 
        /// </summary>
        /// <param name="text">текст к которому необходимо пременить</param>
        /// <returns>текст html страницы</returns>
        public string ApplyTemplate(string text)
        {
            var result = text.Insert(PositionBegin, HtmlTag.ToString());
            var taglength = HtmlTag.ToString().Length;
            if (!HtmlTag.Isclosed)
            {
                HtmlTag.Isclosed = true;
                return result.Insert(PositionEnd() + taglength, HtmlTag.ToString());    
            }
            return result;
        }

        public override string ToString()
        {
            return "(" + PositionBegin + ":" + PositionEnd() + ")" + HtmlTag;
        }
    }
}