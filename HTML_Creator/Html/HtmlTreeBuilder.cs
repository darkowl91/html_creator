﻿using System.Collections;
using System.Globalization;
using System.Windows.Forms;

namespace HTML_Creator.Html
{
    /// <summary>
    /// Дерево тегов
    /// </summary>
    public class HtmlTreeBuilder
    {
        private const string Html = "HTML";
        private const string Title = "Untitled";
        private const string Comment = "Comment";
        private int _nodeCounter;

        /// <summary>
        /// обнуляем счётчик при создани обьекта
        /// </summary>
        public HtmlTreeBuilder()
        {
            this._nodeCounter = 0;
        }

        /// <summary>
        /// Получить дерево элементов html докумнета
        /// </summary>
        /// <param name="document">Документ html</param>
        /// <returns>tree</returns>
        public TreeNode GetTree(HtmlDocument document)
        {
           _nodeCounter = 0;
           var htmElements = document.GetElementsByTagName(Html);
           var rootNode = new TreeNode
               {
                   //если докумен несодержит названия 
                   Text = string.IsNullOrWhiteSpace(document.Title) ? Title : document.Title
               };
            Buildtree(htmElements, rootNode);
           return rootNode;
        }

        private void Buildtree(IEnumerable currentEle, TreeNode tn)
        {
            foreach (HtmlElement element in currentEle)
            {
                
                var tag = element.TagName;
                TreeNode tempNode;
                if (System.String.Compare(tag, "!", System.StringComparison.Ordinal) == 0)
                {
                    tempNode = tn.Nodes.Add(Comment);
                }
                else
                {
                    tempNode = tn.Nodes.Add("<" + element.TagName + ">");
                    
                }
                tempNode.Name = _nodeCounter++.ToString(CultureInfo.InvariantCulture);
                Buildtree(element.Children, tempNode);
            }
        }
    }
}