﻿using System;

namespace HTML_Creator.Html
{
    /// <summary>
    /// базовый класс тега
    /// </summary>
    [Serializable]
    public abstract class Tag
    {
        /// <summary>
        /// Имя тега
        /// </summary>
        public string Name { get; set; }


        protected Tag()
        {
        }

        /// <summary>
        /// конструктор тега 
        /// </summary>
        /// <param name="name">имя тега</param>
        protected Tag(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}