﻿using System;
using System.Windows.Forms;
using HTML_Creator.Forms;

namespace HTML_Creator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new HtmlCreatorForm(System.IO.File.ReadAllText(@"test.txt")));
        }
    }
}
