﻿using System.Collections.Generic;
using HTML_Creator.Commands;

namespace HTML_Creator.util
{
    /// <summary>
    /// Класс для работы с командами
    /// </summary>
    public class CommandHelper
    {
        private static CommandHelper _instance;
        private readonly Dictionary<string, ICommand> _commands;
        
        /// <summary>
        /// инициализация списка команд
        /// </summary>
        private CommandHelper()
        {
            _commands = new Dictionary<string, ICommand>
                {
                    {"BoldItemMenu", new BoldCommand()},
                    {"FontMenuItem", new FontCommand()},
                    {"ImageMenuItem", new ImageCommand()},
                    {"LinkMenuItem", new LinkCommand()},
                    {"TableMenuItem", new TableCommand()},
                    {"ItalicMenuItem", new ItalicCommand()},
                    {"CenteerMenuItem",new CenterCommand()},
                    {"LeftMenuItem", new LeftCommand()},
                    {"RightMenuItem", new RightCommand()},
                    {"LineMenuItem",new LineCommand()},
                    {"UnderlineMenuItem", new UnderlineCommand()}
                };
        }

        /// <summary>
        /// Получить команду по имени
        /// </summary>
        /// <param name="commandName">Имя команды</param>
        /// <returns></returns>
        public ICommand GetCommand(string commandName)
        {
            var command = _commands[commandName] ?? new NoCommand();
            return command;
        }

        /// <summary>
        /// получить обьект класса 
        /// </summary>
        /// <returns></returns>
        public static CommandHelper GetInstance()
        {
            return _instance ?? (_instance = new CommandHelper());
        }
    }
}