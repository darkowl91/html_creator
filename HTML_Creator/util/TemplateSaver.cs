﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using HTML_Creator.Html;

namespace HTML_Creator.util
{
    /// <summary>
    /// класс для сохранения шаблонов содержащих теги
    /// </summary>
    public static class TemplateSaver
    {
        private const string EmptyStr = "";

        /// <summary>
        /// сохраняет список шаблонов в строку xml
        /// сохранение выполняется через сериализацию 
        /// </summary>
        /// <param name="templates">список шаблонов</param>
        /// <returns>строка xml</returns>
        public static string SaveToXml(List<HtmlTemplate> templates)
        {
            var xmlStr = EmptyStr;

            if (templates != null && templates.Count > 0)
            {
                // создаем сериалайзер
                var sr = new XmlSerializer(templates.GetType());
                // создаем writer, в который будет происходить сериализация
                var sb = new StringBuilder();
                var w = new StringWriter(sb, System.Globalization.CultureInfo.InvariantCulture);
                // сериализуем
                sr.Serialize(w, templates);
                // получаем строку Xml
                xmlStr = sb.ToString();
            }
            return xmlStr;
        }

        /// <summary>
        /// получаем список шаблонов из строки xml 
        /// </summary>
        /// <param name="xmlStr">строка xml</param>
        /// <returns>список шаблонов</returns>
        public static List<HtmlTemplate> OpenFromXmlStr(string xmlStr)
        {
            List<HtmlTemplate> templates = null;

            if (!EmptyStr.Equals(xmlStr))
            {
                // создаем reader
                var reader = new StringReader(xmlStr);
                // создаем XmlSerializer
                var dsr = new XmlSerializer(typeof(List<HtmlTemplate>));
                // десериализуем 
                templates = (List<HtmlTemplate>)dsr.Deserialize(reader);
            }
            return templates;
        }
    }
}