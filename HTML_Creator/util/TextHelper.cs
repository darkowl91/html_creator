﻿namespace HTML_Creator.util
{
    /// <summary>
    /// Класс помошник для работы с полученными данными
    /// Синглтон
    /// </summary>
    public class TextHelper
    {
        private static TextHelper _instance;
        private string _textData;
        private string _sourceData;

        /// <summary>
        /// приватный конструктор исключает возможность создать обьект
        /// </summary>
        private TextHelper(){}

        /// <summary>
        /// полученная строка с текстом
        /// </summary>
        public string TextData
        {
           //bug  richTextBox ned to remoove coret return 
            get { return _textData; } //.Replace("\r",""); }
            //get {return _textData; }
            set { _textData = value; }
        }

        /// <summary>
        ///первоначальный текст 
        /// </summary>
        public string SourceData
        {
            //bug  richTextBox ned to remoove coret return 
            get { return _sourceData; } //.Replace("\r", ""); }
            //get {return _textData; }
            set { _sourceData = value; }
        }
       

        /// <summary>
        /// получение текущего обьекта 
        /// </summary>
        /// <returns>TextHelper instance</returns>
        public static TextHelper GetInstance()
        {
            return _instance ?? (_instance = new TextHelper());
        }
    }
}